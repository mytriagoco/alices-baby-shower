import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
/*const client = new SMTPClient({
  user : 'alicesbabyshower@ytritech.com', 
  password : 'alicesbabyshower',
	ssl: true,
  host : 'mail.ytritech.com',
})*/
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'AlicesBabyShower';
  form: FormGroup ;
  name : FormControl = new FormControl('', [Validators.required, Validators.minLength(4)])
  email: FormControl = new FormControl('', [Validators.required, Validators.email])
  message: FormControl =  new FormControl('', [Validators.required,Validators.minLength(4)])
  honeypot: FormControl =  new FormControl('')
  submitted: boolean = false; // show and hide the success message
  isLoading: boolean = false; // disable the submit button if we're loading
  responseMessage!: string; // response message to user

  get f(){

    return this.form.controls;

  }
   /*FormData: FormGroup;*/
  //private builder: FormBuilder
  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.form = this.formBuilder.group({
      name: this.name,
      email : this.email,
      message: this.message,
      honeypot: this.honeypot,
    })

   }

  ngOnInit(): void {

   // response message to user
    /*this.form = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(4)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      message: new FormControl('',[Validators.required,Validators.minLength(4)])
    });*/
  }
  submit(){
    if (this.form.status == "VALID" && this.honeypot.value == ""){
      this.form.disable(); // Disable form if its valid to disable multiple submissions
       var formdata : any = new FormData();
      formdata.append('name', this.form.controls['name'].value)
      formdata.append('message', this.form.controls['message'].value)

      this.isLoading = true; // sending the post request async so it's in progress
      this.submitted = false; // hide the response message on multiple submits
     
      let data = {
        name:this.form.controls['name'].value,
        email:this.form.controls['email'].value,
        message:this.form.controls['message'].value
      }

      this.http.post("https://alices-baby-shower.herokuapp.com/api/email", data).subscribe((response) => {
          this.responseMessage = "Message was sent successfully";
          this.form.reset(); // re enable the form after a success
          this.submitted = true; // show the response message
          this.isLoading = false; // re enable the submit button
          
        },
        (error) => {
          this.responseMessage = "Message was not sent";
          this.form.enable(); // re enable the form after a success
          this.submitted = true; // show the response message
          this.isLoading = false; // re enable the submit button
          
        }
      );
    }
  }
}
    
    
  

