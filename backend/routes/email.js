
const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/user');

router.post('/email', userCtrl.send );  

module.exports = router;